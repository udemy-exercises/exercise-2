using System;

class exercise2
{
   static void Main()
   {
      const int fuelConsumption = 12;
      int hours, distance;
      float result;

      hours = int.Parse(Console.ReadLine());
      distance = int.Parse(Console.ReadLine());
      result = (float)(hours * distance) / (float)fuelConsumption;

      Console.WriteLine(result.ToString("F3"));
   }
}
