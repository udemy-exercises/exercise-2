using System;

class exercise2
{
   static void Main()
   {
      (int code, int amount, float price) product1;
      (int code, int amount, float price) product2;

      product1.code = int.Parse(Console.ReadLine());
      product1.amount = int.Parse(Console.ReadLine());
      product1.price = float.Parse(Console.ReadLine());      

      product2.code = int.Parse(Console.ReadLine());
      product2.amount = int.Parse(Console.ReadLine());
      product2.price = float.Parse(Console.ReadLine());

      float result = (product1.amount * product1.price) + (product2.amount * product2.price);

      Console.WriteLine($"VALOR A PAGAR: R$ {result.ToString("F2")}");
   }
}
