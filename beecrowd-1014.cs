using System;

class exercise2
{
   static void Main()
   {
      int X;
      float Y;
      float result;

      X = int.Parse(Console.ReadLine());
      Y = float.Parse(Console.ReadLine());
      result = X / Y;

      Console.WriteLine($"{result.ToString("F3")} km/l");
   }
}
