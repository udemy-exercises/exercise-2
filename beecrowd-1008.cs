using System;

class exercise2
{
   static void Main()
   {
      int number, hours;
      float payHour, salary;

      number = int.Parse(Console.ReadLine());
      hours = int.Parse(Console.ReadLine());
      payHour = float.Parse(Console.ReadLine());
      salary = hours * payHour;

      Console.WriteLine($"NUMBER = {number}");
      Console.WriteLine($"SALARY = U$ {salary.ToString("F2")}");
   }
}
