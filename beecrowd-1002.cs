using System;

class exercise2
{
   static void Main()
   {
      double A, pi, R;
      pi = 3.14159;
      R = double.Parse(Console.ReadLine());
      A = pi * Math.Pow(R, 2);

      Console.WriteLine($"A={A}");
   }
}
